/* A simple ray tracer */

//#include <stdio.h>
//#include <stdbool.h> /* Needed for boolean datatype */
//#include <math.h>
#include <GUI.h>
#include <color.h>
#include <math.h>
extern unsigned int globalformat ;
extern unsigned short int  *fbase;
extern volatile unsigned char  *CID_Register;
#define min(a,b) (((a) < (b)) ? (a) : (b))

/* Width and height of out image */
#define WIDTH  640
#define HEIGHT 480
#define xoffset 335
#define yoffset 15 
#define xoffset2 100
#define yoffset2 200 

#define BARWIDTH 0
typedef int bool;
#define true 1
#define false 0


/* The vector structure */
typedef struct{
      float x,y,z;
}vector;

/* The sphere */
typedef struct{
        vector pos;
        float  radius;
	int material;
}sphere; 

/* The ray */
typedef struct{
        vector start;
        vector dir;
}ray;

/* Colour */
typedef struct{
	float red, green, blue;
}colour;

/* Material Definition */
typedef struct{
	colour diffuse;
	float reflection;
}material;

/* Lightsource definition */
typedef struct{
	vector pos;
	colour intensity;
}light;

/* Subtract two vectors and return the resulting vector */
static inline vector vectorSub(const vector v1, const vector v2){
	vector result = {v1.x - v2.x, v1.y - v2.y, v1.z - v2.z };
	return result;
}

/* Multiply two vectors and return the resulting scalar (dot product) */
static inline float vectorDot(const vector v1, const vector v2){
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

/* Calculate Vector x Scalar and return resulting Vector*/ 
static inline vector vectorScale(float c, const vector v){
	vector result = {v.x * c, v.y * c, v.z * c };
	return result;
}

/* Add two vectors and return the resulting vector */
static inline vector vectorAdd(const vector v1, const vector v2){
	vector result = {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z };
	return result;
}

/* Check if the ray and sphere intersect */
bool intersectRaySphere(const ray *r, const sphere *s, float *t){
	
	bool retval = false;
	
	/*
	 * Use a bounding box first
	 */
	if (r->dir.x == 0 &&	r->dir.y == 0) {
		if (r->start.x < (s->pos.x - s->radius) ||
			r->start.y < (s->pos.y - s->radius) ||
			r->start.x > (s->pos.x + s->radius) ||
			r->start.y > (s->pos.y + s->radius))
			return false;
	}

	/* A = d.d, the vector dot product of the direction */
	float A = vectorDot(r->dir, r->dir); 
	
	/* We need a vector representing the distance between the start of 
	 * the ray and the position of the circle.
	 * This is the term (p0 - c) 
	 */
	vector dist = vectorSub(r->start, s->pos);
	
	/* 2d.(p0 - c) */  
	float B = 2 * vectorDot(r->dir, dist);
	
	/* (p0 - c).(p0 - c) - r^2 */
	float C = vectorDot(dist, dist) - (s->radius * s->radius);
	
	/* Solving the discriminant */
	float discr = B * B - 4 * A * C;
	
	/* If the discriminant is negative, there are no real roots.
	 * Return false in that case as the ray misses the sphere.
	 * Return true in all other cases (can be one or two intersections)
	 * t represents the distance between the start of the ray and
	 * the point on the sphere where it intersects.
	 */
	if(discr < 0)
		retval = false;
	else{
		float sqrtdiscr = sqrt(discr);
		float t0 = (-B + sqrtdiscr)/(2);
		float t1 = (-B - sqrtdiscr)/(2);
		
		/* We want the closest one */
		if(t0 > t1)
			t0 = t1;

		/* Verify t1 larger than 0 and less than the original t */
		if((t0 > 0.001f) && (t0 < *t)){
			*t = t0;
			retval = true;
		}else
			retval = false;
	}

return retval;
}

#define MAX_SPHERES 4
#define MAX_LIGHTS 4

typedef struct scene_t {
	
	// Sphere definitions.
	int nSpheres;
	sphere spheres[MAX_SPHERES];
	
	// Material definitions.
	material materials[MAX_LIGHTS];
	
	// Light definitions.
	int nLights;
	light lights[MAX_LIGHTS];
	
} scene_t;

static const scene_t scenes[] = {
	
	{
		.nSpheres = 4,
		.spheres = {
			{ .pos = { 100, -60, 200 }, .radius = 50, .material = 0 },
			{ .pos = { -150, -40, 300 }, .radius = 50, .material = 1 },
			{ .pos = { 0, 50, 400 }, .radius = 50, .material = 2 },
			{ .pos = { 0, 0, 1500 }, .radius = 1000, .material = 3 }
		},
		
		.materials = {
			{ .diffuse = { 0.2, 0.4, 1.0 }, .reflection = 0.2 },
			{ .diffuse = { 0.2, 1.0, 0.8 }, .reflection = 0.5 },
			{ .diffuse = { 0.8, 1.0, 0.2 }, .reflection = 0.9 },
			{ .diffuse = { 0.3, 0.5, 0.2 }, .reflection = 0.1 }
		},
		
		.nLights = 3,
		.lights = {
			{ .pos = { 0,  0, 0 }, .intensity = { 1.0, 1.0, 1.0 } },
			{ .pos = { 500, 1000, 0 }, .intensity = { 0.6, 0.7, 1.0 } },
			{ .pos = { -500, -1000, 0 }, .intensity = { 0.3, 0.7, 0.5 } }
		}
	},
	
	{
		.nSpheres = 4,
		.spheres = {
			{ .pos = { 120, -20, 200 }, .radius = 50, .material = 0 },
			{ .pos = { -130, -50, 300 }, .radius = 50, .material = 1 },
			{ .pos = { 40, 20, 400 }, .radius = 50, .material = 2 },
			{ .pos = { 0, 0, 1500 }, .radius = 1000, .material = 3 }
		},
		
		.materials = {
			{ .diffuse = { 0.2, 0.4, 1.0 }, .reflection = 0.2 },
			{ .diffuse = { 0.2, 1.0, 0.8 }, .reflection = 0.5 },
			{ .diffuse = { 0.8, 1.0, 0.2 }, .reflection = 0.9 },
			{ .diffuse = { 0.3, 0.5, 0.2 }, .reflection = 0.1 }
		},
		
		.nLights = 3,
		.lights = {
			{ .pos = { 100, -50, 0 }, .intensity = { 1.0, 1.0, 1.0 } },
			{ .pos = { 600, 700, 0 }, .intensity = { 0.6, 0.7, 1.0 } },
			{ .pos = { -600, -700, 0 }, .intensity = { 0.3, 0.7, 0.5 } }
		}
	},
	
};

	material materials[3];
	sphere spheres[3]; 
	light lights[3];
	

	
	/* Will contain the raw image */
	//unsigned char img[4*WIDTH*HEIGHT];
//	int *img = (int*)0x400000;
//	unsigned int *img = (unsigned int*)0x4000000;

int main_Raytracer(int x1, int x2, int y1, int y2, int scene) {
	
	int i, osa;
	
	ray r;
	char st[15];
	
	unsigned int RED, GREEN, BLUE;
	
	const scene_t *scn = &scenes[scene];
	
	int x, y;
	int xtemp, ytemp;
	int multf = 0;
	unsigned int color; 
	
	// Loop over the pixels.
	for (y = y1; y < y2; y++) {
		for (x = x1; x < x2; x++) {
			
			float dith = (
				  ((x & 1) << 5)
				| ((y & 1) << 4)
				| ((x & 2) << 2)
				| ((y & 2) << 1)
				| ((x & 4) >> 1)
				| ((y & 4) >> 2)
			) * (1.0f/64.0f);
			
			// Current pixel color.
			float pr = 0;
			float pg = 0;
			float pb = 0;
			
			for (osa = 0; osa < 2; osa++) {
				
				int level = 0;
				float coef = 1.0;
				
				float cam_x = ((x - 320) + ((osa >> 0) & 1) * 0.5f) * (1.0f / 320.f);
				float cam_y = ((y - 120) + ((osa >> 0) & 1) * 0.5f) * (1.0f / 320.f);
				
				r.start.x = 0;
				r.start.y = 0;
				r.start.z = -200;
				
				// Aim for a field of view of about 60 degrees.
				r.dir.x = cam_x * 0.5f;
				r.dir.y = cam_y * 0.5f;
				r.dir.z = 1;
				float temp = vectorDot(r.dir, r.dir);
				r.dir = vectorScale(1.0f / sqrt(temp), r.dir);
				
				do {
					/* Find closest intersection */
					float t = 20000.0f;
					int currentSphere = -1;
					
					unsigned int i;
					for (i = 0; i < scn->nSpheres; i++){
						if (intersectRaySphere(&r, &scn->spheres[i], &t))
							currentSphere = i;
					}
					if(currentSphere == -1) break;
					
					vector scaled = vectorScale(t, r.dir);
					vector newStart = vectorAdd(r.start, scaled);
					
					/* Find the normal for this new vector at the point of intersection */
					vector n = vectorSub(newStart, scn->spheres[currentSphere].pos);
					float temp = vectorDot(n, n);
					if(temp == 0) break;
					
					temp = 1.0f / sqrt(temp);
					n = vectorScale(temp, n);
					
					/* Find the material to determine the colour */
					material currentMat = scn->materials[scn->spheres[currentSphere].material];
					
					/* Find the value of the light at this point */
					unsigned int j;
					for(j = 0; j < scn->nLights; j++){
						light currentLight = scn->lights[j];
						vector dist = vectorSub(currentLight.pos, newStart);
						if(vectorDot(n, dist) <= 0.0f) continue;
						float t = sqrt(vectorDot(dist,dist));
						if(t <= 0.0f) continue;
						
						ray lightRay;
						lightRay.start = newStart;
						lightRay.dir = vectorScale((1/t), dist);
						
						/* Calculate shadows */
						bool inShadow = false;
						unsigned int k;
						for (k = 0; k < scn->nSpheres; ++k) {
							if (intersectRaySphere(&lightRay, &scn->spheres[k], &t)){
								inShadow = true;
								break;
							}
						}
						if (!inShadow){
							/* Lambert diffusion */
							float lambert = vectorDot(lightRay.dir, n) * coef; 
							pr += lambert * currentLight.intensity.red * currentMat.diffuse.red;
							pg += lambert * currentLight.intensity.green * currentMat.diffuse.green;
							pb += lambert * currentLight.intensity.blue * currentMat.diffuse.blue;
						}
					}
					/* Iterate over the reflection */
					coef *= currentMat.reflection;
					
					/* The reflected ray start and direction */
					r.start = newStart;
					float reflect = 2.0f * vectorDot(r.dir, n);
					vector tmp = vectorScale(reflect, n);
					r.dir = vectorSub(r.dir, tmp);
					
					level++;
					
				} while ((coef > 0.0f) && (level < 3));
				
			}
			
			// Convert to framebuffer color system, compensating for OSA as well.
			int ir = (int)(pr * (32.0f * 0.5f /* * 0.25*/) + dith);
			int ig = (int)(pg * (64.0f * 0.5f /* * 0.25*/) + dith);
			int ib = (int)(pb * (32.0f * 0.5f /* * 0.25*/) + dith);
			
			// Clamp. Colors can never be negative without negative lights, but
			// reflections can make them more than zero.
			ir = min(ir, 31);
			ig = min(ig, 63);
			ib = min(ib, 31);
			
			// Write to the framebuffer.
			fbase[x + y*WIDTH] = (ir << 11) | (ig << 5) | ib;
			
		}
	}
	
	return 0;
}





/* Ray tracer provides same workload to each individual box*/


int main2_Raytracer(int x1, int x2 ,int y1, int y2){

	int i;
	
	ray r;
	char st[15];

	unsigned int RED, GREEN,BLUE; 

	format_s *f; 
	f = &formats[globalformat];
	
	materials[0].diffuse.red =1 ;
	materials[0].diffuse.green = 0;
	materials[0].diffuse.blue = 0;
	materials[0].reflection = 0.2;
	
	materials[1].diffuse.red = 0;
	materials[1].diffuse.green = 1;
	materials[1].diffuse.blue = 0;
	materials[1].reflection = 0.5;
	
	materials[2].diffuse.red = 0;
	materials[2].diffuse.green = 0;
	materials[2].diffuse.blue = 1;
	materials[2].reflection = 0.9;
	
	spheres[0].pos.x = 200;
	spheres[0].pos.y = 300;
	spheres[0].pos.z = 0;
	spheres[0].radius = 100;
	spheres[0].material = 0;
	
	spheres[1].pos.x = 400;
	spheres[1].pos.y = 400;
	spheres[1].pos.z = 0;
	spheres[1].radius = 100;
	spheres[1].material = 1;
	
	spheres[2].pos.x = 500;
	spheres[2].pos.y = 140;
	spheres[2].pos.z = 0;
	spheres[2].radius = 100;
	spheres[2].material = 2;
	
	lights[0].pos.x = 0;
	lights[0].pos.y = 240;
	lights[0].pos.z = -100;
	lights[0].intensity.red = 1;
	lights[0].intensity.green = 1;
	lights[0].intensity.blue = 1;
	
	lights[1].pos.x = 3200;
	lights[1].pos.y = 3000;
	lights[1].pos.z = -1000;
	lights[1].intensity.red = 0.6;
	lights[1].intensity.green = 0.7;
	lights[1].intensity.blue = 1;

	lights[2].pos.x = 600;
	lights[2].pos.y = 0;
	lights[2].pos.z = -100;
	lights[2].intensity.red = 0.3;
	lights[2].intensity.green = 0.5;
	lights[2].intensity.blue = 1;
	

	
	int x, y;
	int multf = 0;
	int ytemp, xtemp ; 
	unsigned int color; 
	for(y=0 +yoffset2 ;y<120+yoffset2;y++){ 
	
	/* offset is being added in order to move the picture upward, becuase 
	  offset amount of picture does nothing*/
		
		switch(CID_Register[0]){
			case 0:
				color = Green;
				break;
			case 1: 
				color = Blue;
				break;
			case 2: 
				color = Maroon;
				break;
			case 3:
				color = Olive;
				break;
			default:
				puts(" The context Number is not correct \n");
				color = Red;

		}

		
		for(ytemp = y  ;ytemp < 120+yoffset2;ytemp++){ 

			for(xtemp= 0 + xoffset2  ;  xtemp< 160 +xoffset2 ;xtemp++){
				fbase[(xtemp-xoffset2)+(ytemp-yoffset2)*WIDTH + x1 + y1*WIDTH] = color; 
			}
		}
		
		/* The formula in the loop is developed in order to process just one spehere */

		for(x= 0 + xoffset2  ;  x< 160 +xoffset2 ;x++){
			
			float red = 0;
			float green = 0;
			float blue = 0;
			
			int level = 0;
			float coef = 1.0;
			
			r.start.x = x;
			r.start.y = y;
			r.start.z = -2000;
			
			r.dir.x = 0;
			r.dir.y = 0;
			r.dir.z = 1;
			

			
			do{
				/* Find closest intersection */
				float t = 20000.0f;
				int currentSphere = -1;
				
				unsigned int i;
				for(i = 0; i < 3; i++){
					if(intersectRaySphere(&r, &spheres[i], &t))
						currentSphere = i;
				}
				if(currentSphere == -1) break;
				
				vector scaled = vectorScale(t, r.dir);
				vector newStart = vectorAdd(r.start, scaled);
				
				/* Find the normal for this new vector at the point of intersection */
				vector n = vectorSub(newStart, spheres[currentSphere].pos);
				float temp = vectorDot(n, n);
				if(temp == 0) break;
				
				temp = 1.0f / sqrt(temp);
				n = vectorScale(temp, n);

				/* Find the material to determine the colour */
				material currentMat = materials[spheres[currentSphere].material];
				
				/* Find the value of the light at this point */
				unsigned int j;
				for(j=0; j < 3; j++){
					light currentLight = lights[j];
					vector dist = vectorSub(currentLight.pos, newStart);
					if(vectorDot(n, dist) <= 0.0f) continue;
					float t = sqrt(vectorDot(dist,dist));
					if(t <= 0.0f) continue;
					
					ray lightRay;
					lightRay.start = newStart;
					lightRay.dir = vectorScale((1/t), dist);
					
					/* Calculate shadows */
					bool inShadow = false;
					unsigned int k;
					for (k = 0; k < 3; ++k) {
						if (intersectRaySphere(&lightRay, &spheres[k], &t)){
							inShadow = true;
							break;
						}
					}
					if (!inShadow){
						/* Lambert diffusion */
						float lambert = vectorDot(lightRay.dir, n) * coef; 
						red += lambert * currentLight.intensity.red * currentMat.diffuse.red;
						green += lambert * currentLight.intensity.green * currentMat.diffuse.green;
						blue += lambert * currentLight.intensity.blue * currentMat.diffuse.blue;
					}
				}
				/* Iterate over the reflection */
				coef *= currentMat.reflection;
				
				/* The reflected ray start and direction */
				r.start = newStart;
				float reflect = 2.0f * vectorDot(r.dir, n);
				vector tmp = vectorScale(reflect, n);
				r.dir = vectorSub(r.dir, tmp);

				level++;

			}while((coef > 0.0f) && (level < 5));
			
		if (x < WIDTH-BARWIDTH){
							
	
						 
fbase[(x-xoffset2)+(y-yoffset2)*WIDTH + x1 + y1*WIDTH] = ((((char)min(red*32.0f, 32.0f) & 0x1F)<< 11) | (((char)min(green*64.0f, 64.0f) & 0x3F) << 5) | (((char)min(blue*32.0f, 32.0f) & 0x1F))) ;
}	
	else
				fbase[(x -xoffset2) + (y-yoffset2)*WIDTH +x1+y1*WIDTH] = 0x0000; //assuming the compiler recognizes the *8, otherwise change it to <<3
		}
	}

	
return 0;
}
