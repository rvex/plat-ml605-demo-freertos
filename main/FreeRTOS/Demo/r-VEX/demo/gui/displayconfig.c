#include<GUI.h>
#include<color.h>

extern unsigned int globalformat  ;
extern unsigned short int  *fbase; // 16 bit mode is being used 

/*Will display the current hardware Configuration*/


void displayConfig(unsigned int  configWords){

	
	 int x, y; 
	 unsigned int color[5];  
	 unsigned int startx;

	format_s *f;
        f = &formats[globalformat];

        unsigned int width = ( f->hactive_video/8)   ; // This width has been chosen arbitrarily 

	 unsigned int configtuple[4]; 
	unsigned int   spacing;
	unsigned int overAll = 0;
	int loop, spacingLoop;
	unsigned int subDiv = 0 ; 
	 unsigned int lineConfig[4];
	unsigned int C0_Flag = 0 , C1_Flag =0 ,C2_Flag = 0 ,C3_Flag = 0 ,C8_Flag = 0 , C9_Flag = 0 ;
	 unsigned int startxC0, widthC0, startxC1, widthC1, startxC2, widthC2,startxC3, widthC3 , startxC8,widthC8 , startxC9,widthC9;
	spacing = 4;
	startx = 4 ; 
	startxC0 = 0 ; 
	widthC0  = 0 ; 
	startxC1 = 0 ; 
	widthC1  = 0 ; 
	startxC2 = 0 ;
	widthC2  = 0 ; 
	startxC3 = 0 ; 
	widthC3  = 0 ;
	startxC8 = 0 ; 
	widthC8  = 0 ;
	startxC9 = 0 ;
	widthC9  = 0 ; 
	color[0] = Green;
	color[1] = Blue;
	color[2] = Maroon;
	color[3] = Olive;
	color[4] = Black;
	configtuple[0] = configWords & 0x000F ;
	configtuple[1] = (configWords & 0x00F0 ) >> 4 ;
	configtuple[2] = (configWords & 0x0F00) >> 8 ;
	configtuple[3] = (configWords & 0xF000) >> 12 ;


	/* 0x8180 is very special case. so there is need to handle  such case separatly
	   here, there is need for two windows for one type(8). therefore we will make 
	   one of the 8  number into 9 
	*/
	
	if(configtuple[1] == 8 && configtuple[2] != 8){
		if ( configtuple[3] == 8 ){
			configtuple[3] += 1  ;
		}
	}     


	for ( loop  = 0 ; loop < 4 ; loop++){
		switch(configtuple[loop]){
			case 0:
				if(startxC0 == 0){
					widthC0 = widthC0 + width + overAll+spacing;
					overAll =  widthC0;
					startxC0 = startx ; 
					startx   = startx + width + spacing;
					lineConfig[subDiv] = widthC0 + 20;
					++subDiv;
				}
				else{
					widthC0 = widthC0 + width+spacing ;
					startx   = startx + width +spacing;
					overAll =  widthC0 ;
					lineConfig[subDiv - 1 ] = widthC0 + 20;
				}
				C0_Flag = 1; 
				break;
			case 1: 
				if(startxC1 == 0){
					widthC1 = widthC1 + width +overAll +spacing;
					overAll = widthC1 ;
					startxC1 = startx ; 
					startx   = startx + width + spacing;
					lineConfig[subDiv] = widthC1 + 20; 
					++subDiv;
			
				}
				else{
					widthC1 = widthC1 + width+spacing;
					startx   = startx + width +spacing;
					overAll = widthC1;
					lineConfig[subDiv -1] = widthC1 + 20;
				}
				C1_Flag = 1 ; 
				break;
			case 2: 
				if(startxC2 == 0){
					widthC2 = widthC2 + width + overAll + spacing;
					overAll = widthC2 ; 
					startxC2 = startx ; 
					startx   = startx + width + spacing;
					lineConfig[subDiv] = widthC2 +20 ;
					++subDiv;
					
				}
				else{
					widthC2 = widthC2 + width+spacing;
					startx   = startx + width +spacing;
					overAll = widthC2; 
					lineConfig[subDiv -1 ] = widthC2 +20;
				}
				C2_Flag = 1 ; 
				break;
			case 3:
				if(startxC3 == 0){
					widthC3 = widthC3 + width + overAll + spacing;
					overAll = widthC3 ;
					startxC3 = startx ; 
					startx   = startx + width + spacing;
					lineConfig[subDiv] = widthC3 + 20;
					++subDiv;
				}
				else{
					widthC3 = widthC3 + width+spacing;
					startx   = startx + width +spacing;
					overAll = widthC3;
					lineConfig[subDiv -1 ] = widthC3 +20;
				}
				C3_Flag = 1 ; 
				break; 
			case 8: 
				if(startxC8 == 0){
					widthC8 = widthC8 + width + overAll + spacing;
					overAll = widthC8;
					startxC8 = startx ; 
					startx   = startx + width + spacing;
					lineConfig[subDiv] = widthC8 + 20;
					++subDiv;
				}
				else{
					widthC8 = widthC8 + width+spacing;
					startx   = startx + width +spacing;
					overAll = widthC8;
					lineConfig[subDiv -1 ] = widthC8 +20;
				}
				C8_Flag = 1; 
				break;
			case 9: 
				if(startxC9 == 0){
					widthC9 = widthC9 + width + overAll + spacing;
					overAll = widthC9;
					startxC9 = startx ; 
					startx   = startx + width + spacing;
					lineConfig[subDiv] = widthC9 + 20;
					++subDiv;
				}
				else{
					widthC9 = widthC9 + width+spacing;
					startx   = startx + width +spacing;
					overAll = widthC9;
					lineConfig[subDiv -1 ] = widthC9 +20;
				}
				C9_Flag = 1; 
				break;
			default:
				puts(" configuration words is not valid\n");
		}
	}
	
	// The area to draw Grey lines to show division between the configuration
		/*neConfig[0] = width+ 26 ;            // width + gap b/w grey and start of screen + gap b/w start of grey area to start of acutal color = 4+20+4
		lineConfig[1] = 2*width +spacing + 26; 
		lineConfig[2] = 3*width +2*spacing + 26;*/

	 for(x = 0; x < subDiv ; x++) {
        	for(y = f->vactive_video/2+1; y < (3* f->vactive_video/4); y++) {
			for(spacingLoop = 0 ; spacingLoop < spacing ; spacingLoop++)
           		fbase[(y* f->hactive_video) + lineConfig[x] +spacingLoop + (40* f->hactive_video) ] = LightGrey; /*  twice same statement is used to make grey line thick*/
			//fbase[(y* f->hactive_video) + lineConfig[x] +1+ (40* f->hactive_video)  ] = LightGrey;

        	}
     	}

    	
	/* The following piece of code show the hardware configuration  with width
	 * of each context acccoding to its issue width   */ 
     	
	
	if( C0_Flag == 1 ){
		 for(y =  f-> vactive_video/2 + 4 ; y  < (3*f-> vactive_video/4) - 4 ;y++) {
                	for(x = startxC0; x < widthC0; x++) {
                        	fbase[ f->hactive_video * y  + x + 40*f->hactive_video +20  ] = color[0];
        		}
     		}
	}	
	
	if(C1_Flag == 1){
 		 for(y =  f-> vactive_video/2 + 4 ; y  < (3*f-> vactive_video/4) - 4 ;y++) {
                	for(x = startxC1; x <  widthC1; x++) {
                        	fbase[ f->hactive_video * y  + x + 40*f->hactive_video +20  ] = color[1];
        		}
     		}
	}
	
	if(C2_Flag == 1){
  		for(y =  f-> vactive_video/2 + 4 ; y  < (3*f-> vactive_video/4) - 4 ;y++) {
                	for(x = startxC2; x < widthC2; x++) {
                        	fbase[ f->hactive_video * y  + x + 40*f->hactive_video +20  ] = color[2];
        		}
     		}
	}
	
	if(C3_Flag == 1){
  		for(y =  f-> vactive_video/2 + 4 ; y  < (3*f-> vactive_video/4) - 4 ;y++) {
                	for(x = startxC3; x < widthC3 ; x++) {
                        	fbase[ f->hactive_video * y  + x + 40*f->hactive_video +20  ] =color[3];
        		}
     		}
	}

	if(C8_Flag == 1 ){
  		for(y =  f-> vactive_video/2 + 4 ; y  < (3*f-> vactive_video/4) - 4 ;y++) {
                	for(x = startxC8; x < widthC8 ; x++) {
                        	fbase[ f->hactive_video * y  + x + 40*f->hactive_video +20  ] = color[4];
        		}
     		}
	}
	
	if(C9_Flag == 1 ){
		 for(y =  f-> vactive_video/2 + 4 ; y  < (3*f-> vactive_video/4) - 4 ;y++) {
                        for(x = startxC9; x < widthC9 ; x++) {
                                fbase[ f->hactive_video * y  + x + 40*f->hactive_video +20  ] = color[4];
                        }
                }
        }

	
}

