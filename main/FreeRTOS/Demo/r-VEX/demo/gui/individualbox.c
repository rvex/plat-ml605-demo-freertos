/*As from the initialization, we have divided the upper half part of the screen into
* into 8 boxes here any processing that is related to the individual box will be 
performed
*/

#include<GUI.h>
#include<color.h>
extern unsigned int globalformat;
extern unsigned short int *fbase; // 16 bit mode is being used 
extern unsigned int switchModeFlag;

extern int main_Raytracer(int x1, int x2, int y1, int y2, int scene);

void processIndividualBox(unsigned int box, unsigned int context, int scene) {
	
	int x, y, x1, x2, y1, y2, w, h;
	unsigned int color;
	format_s *f;
	
	f = &formats[globalformat];
	
	switch (context) {
		case 0:
			color = Green;
			break;
		case 1: 
			color = Blue;
			break;
		case 2: 
			color = Maroon;
			break;
		case 3:
			color = Olive;
			break;
		default:
			puts(" The context Number is not correct \n");
			color = Red;
	}
	
	w = f->hactive_video >> 2;
	h = f->vactive_video >> 2;
	
	x1 = box & 3;
	y1 = (box >> 2) & 1;
	
	x1 *= w;
	x2 = x1 + w - ((box & 3) != 3);
	
	y1 *= h;
	y2 = y1 + h - ((box & 4) != 4);
	
	for (y = y1; y < y2; y++) {
		for (x = x1; x < x2; x++) {
			fbase[f->hactive_video * y + x] = color;
		}
	}
	
	main_Raytracer(x1, x2, y1, y2, scene);
	/*
	
	switch (box) {

	case 1: 
		// 1st box
		for (y = 0; y < f-> vactive_video/4; y++) {
			for (x = 0; x < f->hactive_video/4; x++) {
	fbase[ f->hactive_video * y + x ] = color;
	}
	}

	if(switchModeFlag == 1)
	main_Raytracer(0, f->hactive_video/4, 0, f->vactive_video/4 );
	else 
	main2_Raytracer(0, f->hactive_video/4, 0, f->vactive_video/4 );

	break;
	case 2: 
	// 2nd box 
	for (y = 0; y < f-> vactive_video/4; y++) {
	for (x = f->hactive_video/4; x < f->hactive_video/2; x++) {
	fbase[ f->hactive_video * y + x ] = color;
	}
	}

	if(switchModeFlag == 1)
	main_Raytracer(f->hactive_video/4, f->hactive_video/2, 0, f->vactive_video/4 );
	else 
	main2_Raytracer(f->hactive_video/4, f->hactive_video/2, 0, f->vactive_video/4 );
	break;
	case 3: 
	// 3rd box
	for (y = 0; y < f-> vactive_video/4; y++) {
	for (x = f -> hactive_video/2; x < 3*f->hactive_video/4; x++) {
	fbase[ f->hactive_video * y + x ] = color;
	}
	}

	if(switchModeFlag == 1)
	main_Raytracer(f->hactive_video/2, 3*f->hactive_video/4, 0, f->vactive_video/4 );
	else	
	main2_Raytracer(f->hactive_video/2, 3*f->hactive_video/4, 0, f->vactive_video/4 );

	break;
	case 4: 
	// 4th Box 
	for (y = 0; y < f-> vactive_video/4; y++) {
	for (x = 3*f->hactive_video/4; x < f->hactive_video; x++) {
	fbase[ f->hactive_video * y + x ] = color;
	}
	}


	if(switchModeFlag == 1)
	main_Raytracer(3*f->hactive_video/4, f->hactive_video, 0, f->vactive_video/4 );
	else
	main2_Raytracer(3*f->hactive_video/4, f->hactive_video, 0, f->vactive_video/4 );

	break;

	case 5: 
	// 5th box 
	for (y = f-> vactive_video/4; y < f-> vactive_video/2; y++) {
	for (x=0; x < f->hactive_video/4; x++) {
	fbase[ f->hactive_video * y + x ] = color;
	}
	}

	if(switchModeFlag == 1)
	main_Raytracer(0, f->hactive_video/4, f->vactive_video/4, f->vactive_video/2 );
	else
	main2_Raytracer(0, f->hactive_video/4, f->vactive_video/4, f->vactive_video/2 );

	break;

	case 6: 
	// 6th box 

	for (y = f-> vactive_video/4; y < f-> vactive_video/2; y++) {
	for (x = f->hactive_video/4; x < f->hactive_video/2; x++) {
	fbase[ f->hactive_video * y + x ] = color;
	}
	}

	if(switchModeFlag == 1)
	main_Raytracer(f->hactive_video/4, f->hactive_video/2, f->vactive_video/4, f->vactive_video/2);
	else
	main2_Raytracer(f->hactive_video/4, f->hactive_video/2, f->vactive_video/4, f->vactive_video/2);

	break;

	case 7: 
	// 7th box 

	for (y = f-> vactive_video/4; y < f-> vactive_video/2; y++) {
	for (x = f -> hactive_video/2; x < 3*f->hactive_video/4; x++) {
	fbase[ f->hactive_video * y + x ] = color;
	}
	}

	if(switchModeFlag == 1)
	main_Raytracer(f->hactive_video/2, 3*f->hactive_video/4, f->vactive_video/4, f->vactive_video/2 );
	else 
	main2_Raytracer(f->hactive_video/2, 3*f->hactive_video/4, f->vactive_video/4, f->vactive_video/2 );

	break;

	case 8: 
	// 8th box 
	for (y = f-> vactive_video/4; y < f-> vactive_video/2; y++) {
	for (x = 3*f->hactive_video/4; x < f->hactive_video; x++) {
	fbase[ f->hactive_video * y + x ] = color;
	}
	}

	if(switchModeFlag ==1 )
	main_Raytracer(3*f->hactive_video/4, f->hactive_video, f->vactive_video/4, f->vactive_video/2);
	else 
	main2_Raytracer(3*f->hactive_video/4, f->hactive_video, f->vactive_video/4, f->vactive_video/2);

	break;



	default: 
	puts(" Please Enter Box Number between 1 to 8 \n");
	}
	*/
}
