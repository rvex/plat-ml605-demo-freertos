// Modified by Muhammad Muneeb Yousaf 

/* With 'real' set to a non-zero value:
 * This application has a number of supported formats that are listed 
 * in the "formats" array below. The core's pixelclocks are matched 
 * against the period required for each format. As soon as a match has
 * been found a framebuffer is allocated and cleared. The core is then 
 * enabled  and a testscreen is drawn in the framebuffer. 
 *
 * With 'real' set to zero:
 * The application choses the first available clock and draws a 
 * small screen.
 *
 */

#include <stdio.h>
#include <GUI.h>
#include <stdlib.h>
#include <malloc.h>
#include "color.h"
#include <math.h>

/* Status register field positions */

unsigned int globalformat = 2 ; 
unsigned short int  *fbase; // 16 bit mode is being used 

/*
 * svgactrl_test(...)
 *
 * Arguments:
 *
 * addr      - Address of core registers
 * real      - See description at top of file
 * alloc     - Allocate memory dynamically, otherwise fbaddress is used
 * fbaddress - Frame buffer address
 * format    - Selects format or -1 for autodetect
 * delay     - Counter that delays core disable, -1 to delay forever
 * blank     - Blank screen before core enable 
 */

int svgactrl_test(unsigned int addr, unsigned int real, unsigned int alloc, 
                  unsigned int fbaddress, int format, int delay, int blank){
  		int found = 0;
  		int clock;
		//unsigned int *fbase;

 		 format_s *f;

  		 struct svgactrlregs *regs;
 		 globalformat = format; 

  		regs = (struct svgactrlregs*)(addr);

  		regs->stat = 0;


 		 if (real) {
     			if (format == -1) {
        		/* Scan available clocks */
        			for (format = 0; format < NUM_FORMATS && !found; format++) {
          				 for (clock = 0; clock < 4; clock++) {
              					if (formats[format].period == regs->dclock[clock]) {
                 					found = 1;
                 					break;
             					 }
           				}
        			}
     			} else {
        		/* Check if clock for format is available */
       				 for (clock = 0; clock < 4; clock++) {
           				if (formats[format].period == regs->dclock[clock]) {
              					found = 1;
              					break;
           				}
       				 }		
     			}
  		} else {
     			/* Must have a clock with period != 0 */
     				format = TEST_FORMAT;
     				for (clock = 0; clock < 4; clock++) {
       					 if (regs->dclock[clock]) {
           					found = 1;
          					 break;
        				 }
     				}
  		}

  		if (!found) {
			return 1; 
  		}

  
 		 f = &formats[format];

  		regs->vidlen = ((f->vactive_video-1) << 16)  | (f->hactive_video-1);
  		regs->fporch = ((f->vfporch) << 16)  | (f->hfporch);
 		regs->synclen = ((f->vsync) << 16)  | (f->hsync);
 		regs->linelen = (((f->vactive_video-1 + f->vfporch + f->vsync + f->vbporch) << 16) |
                 (f->hactive_video-1 + f->hfporch + f->hsync + f->hbporch));
 
 		if (alloc) {
     		/* Allocate framebuffer */
   		//fbase = memalign(1024, 2*f->hactive_video*f->vactive_video);
     			if (fbase == NULL) {
    				 //   fail(1);
				return 1;
    			 }
  		} else {
    			 /* Use specific frame buffer base address */
    			 fbase = (int*)fbaddress;
  		}
  		regs->framebuf = (int)fbase;
  

  	/* Enable controller */
  		regs->stat = ((clock << SVGACTRL_CLKSEL) | (2 << SVGACTRL_BDSEL) |
                	(1 << SVGACTRL_EN));
   
     
    

  return 0;
}

