/*
 FreeRTOS V8.2.3 - Copyright (C) 2015 Real Time Engineers Ltd.
 All rights reserved

 VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

 This file is part of the FreeRTOS distribution.

 FreeRTOS is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License (version 2) as published by the
 Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

 ***************************************************************************
 >>! NOTE: The modification to the GPL is included to allow you to !<<
 >>! distribute a combined work that includes FreeRTOS without being !<<
 >>! obliged to provide the source code for proprietary components !<<
 >>! outside of the FreeRTOS kernel. !<<
 ***************************************************************************

 FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. Full license text is available on the following
 link: http://www.freertos.org/a00114.html

 ***************************************************************************
 * *
 * FreeRTOS provides completely free yet professionally developed, *
 * robust, strictly quality controlled, supported, and cross *
 * platform software that is more than just the market leader, it *
 * is the industry's de facto standard. *
 * *
 * Help yourself get started quickly while simultaneously helping *
 * to support the FreeRTOS project by purchasing a FreeRTOS *
 * tutorial book, reference manual, or both: *
 * http://www.FreeRTOS.org/Documentation *
 * *
 ***************************************************************************

 http://www.FreeRTOS.org/FAQHelp.html - Having a problem? Start by reading
 the FAQ page "My application does not run, what could be wrong?". Have you
 defined configASSERT()?

 http://www.FreeRTOS.org/support - In return for receiving this top quality
 embedded software for free we request you assist our global community by
 participating in the support forum.

 http://www.FreeRTOS.org/training - Investing in training allows your team to
 be as productive as possible as early as possible. Now you can receive
 FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
 Ltd, and the world's leading authority on the world's leading RTOS.

 http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
 including FreeRTOS+Trace - an indispensable productivity tool, a DOS
 compatible FAT file system, and our tiny thread aware UDP/IP stack.

 http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
 CreatedTaskHandle Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

 http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
 Integrity Systems ltd. to sell under the OpenRTOS brand. Low cost OpenRTOS
 licenses offer ticketed support, indemnification and commercial middleware.

 http://www.SafeRTOS.com - High Integrity Systems also provide a safety
 engineered and independently SIL3 certified version for use in safety and
 mission critical applications that require provable dependability.

 1 tab == 4 spaces!
*/


/* Scheduler include files. */
#include "FreeRTOS.h"
//#include<stdlib.h>
#include "task.h"
#include "color.h"
#include <string.h> 


/*********** External Definations*****************/

extern TaskHandle_t CreatedTaskHandle[configMAX_PRIORITIES]; 
/*-----------------------------------------------------------*/

extern unsigned int configWords; 
extern unsigned int currentMode;
unsigned int previousMode = 0;
#define trace_Address 0xD0002000
volatile unsigned int *CSC1_Register = (unsigned int*)CR_CSC1_ADDR;
volatile unsigned int *CSC2_Register = (unsigned int*)CR_CSC2_ADDR;
volatile unsigned int *CSC3_Register = (unsigned int*)CR_CSC3_ADDR;

volatile unsigned int *CNT_Register = (unsigned int*)CR_CNT_ADDR;

unsigned long int CRR_Reconfig[4];
unsigned long int Release[4]; 
unsigned long int Mask [4] ={0xFFF0,0xFF0F,0xF0FF,0x0FFF};
unsigned int currentCRR = 0; 
unsigned int clearScreenFlag = pdFALSE;
extern unsigned int highestPriorityTaskPeriod; 
extern unsigned int taskPeriod; 
/*just for the Visualization purpose */

#define BUFFERLIMIT 100
unsigned int vData[4][100];
void vaddData(unsigned int);

unsigned int indexIn[4] = {0, 0, 0, 0 }; 
unsigned int qSize[4] = {0, 0, 0, 0};
////////////////////////////////////////////////

// Declaration of the Tasks 
static void raytracer1(void *data);
static void raytracer2(void *data);
static void raytracer3(void *data);
static void raytracer4(void *data);
static void raytracer5(void *data);
static void raytracer6(void *data);
static void raytracer7(void *data);
static void raytracer8(void *data);

/********************************/
/* ***Auxiliary Position *******/
/******************************/ 
int waitPoint(void);
// Definations of priporities with respest to the idle task
#define dummyTask1_TASK_PRIORITY (tskIDLE_PRIORITY + 1)
#define dummyTask2_TASK_PRIORITY (tskIDLE_PRIORITY + 2)
#define dummyTask3_TASK_PRIORITY (tskIDLE_PRIORITY + 3)
#define dummyTask4_TASK_PRIORITY (tskIDLE_PRIORITY + 4)
#define dummyTask5_TASK_PRIORITY (tskIDLE_PRIORITY + 5)
#define dummyTask6_TASK_PRIORITY (tskIDLE_PRIORITY + 6)
#define dummyTask7_TASK_PRIORITY (tskIDLE_PRIORITY + 7)
#define dummyTask8_TASK_PRIORITY (tskIDLE_PRIORITY + 8)
#define dummyTask9_TASK_PRIORITY (tskIDLE_PRIORITY + 9)
#define dummyTask10_TASK_PRIORITY (tskIDLE_PRIORITY + 10)

#define fullRange 0xFFFFFFFF
#define requestedResource 0x1
#define requested2Resource 0x2
#define requested4Resource 0x4

struct timerregs {
	volatile unsigned int scalercnt;
	volatile unsigned int scalerload;
	volatile unsigned int control;
	volatile unsigned int unused;
	volatile unsigned int timercnt1;
	volatile unsigned int timerload1;
	volatile unsigned int timerctrl1;
	volatile unsigned int unused2;
	volatile unsigned int timercnt2;
	volatile unsigned int timerload2;
	volatile unsigned int timerctrl2;
};
#define TIMER_BASE_ADDRESS 0x80000300


struct irqregs {
	volatile unsigned int irqlevel;
	volatile unsigned int irqpend;
	volatile unsigned int irqforce;
	volatile unsigned int irqclear;
	volatile unsigned int pad[12];
	volatile unsigned int irqmask;
};
#define IRQCTRL_BASE_ADDRESS 0x80000200

unsigned int rand(void) {
	static volatile unsigned short lfsr = 0xACE1u;
	static volatile unsigned int bit;
	bit = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5)) & 1;
	return lfsr = (lfsr >> 1) | (bit << 15);
}

int main(void) {
	int a = 12;
	char st[15];
	tohex(st, a);
	puts("Start of FreeRTOS demo main\n");
	puts(st);

	struct irqregs *iregs = (struct irqregs*) IRQCTRL_BASE_ADDRESS;
	iregs->irqlevel = 0;
	iregs->irqmask = (unsigned long)0xfffe0000;
	iregs->irqmask = iregs->irqmask | 1<<8;
	struct timerregs *tregs = (struct timerregs*) TIMER_BASE_ADDRESS;
	tregs->scalercnt = 32;
	tregs->scalerload = 32;
	tregs->timercnt1 = 1000000;
	tregs->timerload1 = 1000000; //HZ = 1
	tregs->timerctrl1 = 0x0f; //IRQen, Load, auto reload, enable

	svgactrl_test(0x80000600,1,0,0x800000,2,-1, 1);
	initDisplayScreen();
	while (
		((CRR_Reconfig[1] & 0x00F0)!= 0x000) ||
		((CRR_Reconfig[2] & 0x0F00) != 0x0000)|| 
		((CRR_Reconfig[3] & 0xF000)!= 0x000)
	);
	CR_CRR = 0x0000;
	initKernelVariables();
	controlLayerInit();
	
	xTaskCreate(raytracer1, "dummy1", configMINIMAL_STACK_SIZE, NULL, dummyTask1_TASK_PRIORITY, &CreatedTaskHandle[dummyTask1_TASK_PRIORITY],requested2Resource);
	xTaskCreate(raytracer2, "dummy2", configMINIMAL_STACK_SIZE, NULL, dummyTask2_TASK_PRIORITY, &CreatedTaskHandle[dummyTask2_TASK_PRIORITY],requested2Resource);
	xTaskCreate(raytracer3, "dummy3", configMINIMAL_STACK_SIZE, NULL, dummyTask3_TASK_PRIORITY, &CreatedTaskHandle[dummyTask3_TASK_PRIORITY],requested2Resource);
	xTaskCreate(raytracer4, "dummy4", configMINIMAL_STACK_SIZE, NULL, dummyTask4_TASK_PRIORITY, &CreatedTaskHandle[dummyTask4_TASK_PRIORITY],requested2Resource);
	xTaskCreate(raytracer5, "dummy5", configMINIMAL_STACK_SIZE, NULL, dummyTask5_TASK_PRIORITY, &CreatedTaskHandle[dummyTask5_TASK_PRIORITY],requested2Resource);
	xTaskCreate(raytracer6, "dummy6", configMINIMAL_STACK_SIZE, NULL, dummyTask6_TASK_PRIORITY, &CreatedTaskHandle[dummyTask6_TASK_PRIORITY],requested2Resource);
	xTaskCreate(raytracer7, "dummy7", configMINIMAL_STACK_SIZE, NULL, dummyTask7_TASK_PRIORITY, &CreatedTaskHandle[dummyTask7_TASK_PRIORITY],requested2Resource);
	xTaskCreate(raytracer8, "dummy8", configMINIMAL_STACK_SIZE, NULL, dummyTask8_TASK_PRIORITY, &CreatedTaskHandle[dummyTask8_TASK_PRIORITY],requested2Resource);
	
	vTaskStartScheduler();
	return 0;
}

/**
 * Raytracer task.
 */
#define RAYTRACER(i) \
static void raytracer ## i (void *data) { \
	 \
	TickType_t xLastWakeTime = 0; \
	 \
	while (1) { \
		 \
		/* Before starting the actual task, request a different amount of resources. */ \
		int requested = (rand() & 3) + 1; \
		if (requested > 2) { \
			requested = 4; \
		} \
		vTaskResourceSet(requested); \
		 \
		/* Run the raytracer. */ \
		processIndividualBox(8 - i, CR_CID, 0); \
		 \
		/* Delay until next cycle. */ \
		vTaskDelayUntil(&xLastWakeTime, 60); \
		 \
		 \
		/* Before starting the actual task, request a different amount of resources. */ \
		requested = (rand() & 3) + 1; \
		if (requested > 2) { \
			requested = 4; \
		} \
		vTaskResourceSet(requested); \
		 \
		/* Run the raytracer. */ \
		processIndividualBox(8 - i, CR_CID, 1); \
		 \
		/* Delay until next cycle. */ \
		vTaskDelayUntil(&xLastWakeTime, 60); \
		 \
	} \
	 \
}

RAYTRACER(1)
RAYTRACER(2)
RAYTRACER(3)
RAYTRACER(4)
RAYTRACER(5)
RAYTRACER(6)
RAYTRACER(7)
RAYTRACER(8)

/**********************************************************************
****** waitPoint will be used by all other contexts excepts Context0**
***********************************************************************/

int waitPoint(void) {
	char st[15];
	while (1) {
		Release[CR_CID] = CR_CRR;
		Release[CR_CID] = Release[CR_CID] & Mask[CR_CID];
		CRR_Reconfig[CR_CID] = Release[CR_CID];
	}
}

/*********************************************************************/
/***** cyclic Queues for the storage of Data ***********************/
/*******************************************************************/


void vAddData(unsigned int dataElement) {
	vData[CR_CID] [indexIn[CR_CID]] = dataElement; 
	++qSize[CR_CID]; 
	++indexIn[CR_CID]; 
	
	if(indexIn [CR_CID] >= BUFFERLIMIT) {
		indexIn[CR_CID] = 0; 
	}
}

uint32_t _flip_32h_smul_32_16(int32_t a, int32_t b) {
	int64_t t0 = a;
	int64_t t1 = b;
	int64_t t2 = (t0 * (t1 >> 16)) >> 16;
	return t2;
}
