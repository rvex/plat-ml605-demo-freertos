#include "FreeRTOS.h"
#include "task.h"
#include "platform.h"
#define unusedContext 100
/**/


unsigned  int *RSC_Register[4]= {(unsigned int)0,(unsigned int)CR_RSC1_ADDR,(unsigned int)CR_RSC2_ADDR,(unsigned int)CR_RSC3_ADDR};
volatile unsigned  int *WCFG_Register = (unsigned int*)CR_WCFG_ADDR;
volatile unsigned  int *SAWC_Register = (unsigned int*)CR_SAWC_ADDR;

//volatile unsigned char   *CID_Register = (unsigned char *)CR_CID_ADDR;
volatile unsigned long int *CRR_Register = ( unsigned int* )CR_CRR_ADDR;

unsigned int configWords= 0x8888; // Initial state 
UBaseType_t availableCores  = 0x04;;// As, At maximum, foure cores are avaiable so
UBaseType_t availableLaneGroups = 0x1111;// As, At maximum, foure cores are avaiable so

extern TCB_t * volatile pxCT0TaskTCB ; // In the modified implementation pxCurrentTCB will only contain the TCB of the lowest priority running task
extern   phyResource vHardwareContextToPhysicalResourceMapping[HardwareContexts] ;// This data structure contains TCBs of currently running tasks 
extern  TCB_t* volatile  vSotwareContextToHardwareContextMapping[HardwareContexts]; 
extern unsigned int orderedContexts[HardwareContexts];
extern   UBaseType_t  contextSync[HardwareContexts];
/*if some context have higher priority task  than the context zero then force context with the minimum resources*/
struct cacheAffinity {
			unsigned int taskId;
			unsigned int allocatedCores;
			unsigned int laneGroups;
			};
struct cacheAffinity PSAR[HardwareContexts];
//unsigned int preHighestMask = 0 ;
//unsigned int preHighestPriorityCores = 0 ;


void controlLayerInit(){

	unsigned int i ; 

	for( i=0 ; i < HardwareContexts; i++){
		PSAR[i].taskId = unusedContext;
		PSAR[i].allocatedCores = 0x0000;
		PSAR[i].laneGroups= 0x0000;
	}

}

void resetPreviousState(unsigned int Id){

	  PSAR[orderedContexts[Id]].taskId = unusedContext;
          PSAR[orderedContexts[Id]].allocatedCores = 0x0000;
          PSAR[orderedContexts[Id]].laneGroups= 0x0000;
         
}


void retrieveResources(void)
{
	unsigned int config_mask ;
	unsigned int config8_mask;
	unsigned int i ; 

 	for ( i = 0 ; i < HardwareContexts   ; i++){
		if ( vSotwareContextToHardwareContextMapping[i] == NULL){
			 /*puts(" Before released lane groups");
                           tohex(st,availableLanGroups);
                           puts(st);

                           puts(" before released Config Words");
                           tohex(st,configWords);
                           puts(st);*/
			 config_mask = 0xF;
			 config8_mask = 0x08;
			 config_mask = config_mask * vHardwareContextToPhysicalResourceMapping[i].allocatedLanGroups;
			 config8_mask = config8_mask*vHardwareContextToPhysicalResourceMapping[i].allocatedLanGroups;
			 configWords = configWords & (~config_mask);
			 configWords = configWords | (config_mask & config8_mask) ;
			 availableCores  = availableCores + vHardwareContextToPhysicalResourceMapping[i].allocatedPhysicalResource;
			 vHardwareContextToPhysicalResourceMapping[i].allocatedPhysicalResource = 0;
			 availableLaneGroups = availableLaneGroups | vHardwareContextToPhysicalResourceMapping[i].allocatedLanGroups ;
			 vHardwareContextToPhysicalResourceMapping[i].allocatedLanGroups  = 0 ;
		}
	}

}

void controlLayer(){

	unsigned int j,k,i;
	unsigned int resourceAcquired = pdFALSE;
	unsigned int temp_config;
	unsigned int tempMask;
	unsigned int tempSwitch;
	unsigned int tempCores = 0x04;

	char st[15];
	unsigned int uHC = 0x0000;
	unsigned int tempuHC = 0x000;

	
	for( j = 0 ; j < HardwareContexts ; j++){
		
		if(orderedContexts[j] != unusedContext){



			/* 1. Rank of the current task is determined*/

			/* Based upon required resources  mask and config_mask is slected.. These are subsequently 
		 	 * used to assign Hardware LANE groups*/

   			UBaseType_t victimCores = 0;// As, At maximum, foure cores are avaiable so
        		UBaseType_t victimLaneGroups=0; // As, At maximum, foure cores are avaiable so
			UBaseType_t cacheAffinityMask = 0xFFFF;
		 	
				
				
			availableCores 	 += 	vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxAllocatedResource;
			availableLaneGroups  =availableLaneGroups |vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxAllocatedLane;
			
			if(vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource <= availableCores){ 
																		   
			

															   
				/* if avaiable resources are less than the requested ones..
			 	 * so assign  the best possible number of resource . but here
			 	 * original requested recources remains intact 
				*/

					
				if (vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxAllocatedLane == 0){
					tempMask =  vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource; 
                                	cacheAffinityMask = 0xFFFF;
				}
				else
				   if(vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource == 
						vSotwareContextToHardwareContextMapping[orderedContexts[j]] -> uxAllocatedResource){ 
					tempMask= vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource;
				//	availableLaneGroups  =availableLaneGroups |vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxAllocatedLane;
 
                                        cacheAffinityMask = vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxAllocatedLane * 0xF;
				   }
				   else {
						
				   		if(vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource <
							vSotwareContextToHardwareContextMapping[orderedContexts[j]] -> uxAllocatedResource){ 
							 unsigned int temp12 = vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxAllocatedLane;
						//	availableCores = availableCores + vSotwareContextToHardwareContextMapping[orderedContexts[j]] -> uxAllocatedResource;
						//	availableLaneGroups  =availableLaneGroups |temp12;
						
							configWords = configWords  & ~(temp12*0xF);
                                                        configWords = configWords |  temp12*8;
							cacheAffinityMask =  temp12*0xF; 
							tempMask =  vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource;
						//	puts(" small allocated");
						}
						
				   		if(vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource >
							vSotwareContextToHardwareContextMapping[orderedContexts[j]] -> uxAllocatedResource){ 
							 unsigned int temp12 = vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxAllocatedLane;
						//	availableCores = availableCores + vSotwareContextToHardwareContextMapping[orderedContexts[j]] -> uxAllocatedResource;
						//	availableLaneGroups  =availableLaneGroups |temp12;
							configWords = configWords  & ~(temp12*0xF);
                                                        configWords = configWords |  temp12*8;
							cacheAffinityMask =  availableLaneGroups*0xF;  // here still needs some consideration becuase it may jump to other lane groups
							tempMask =  vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource;
								// than the adjacent ones 
						}
					}
			}
			else{
				
				victimLaneGroups= 0x0000;
				victimCores  =0x0000;
				tempuHC = 0x000;
	
				/* Here, we calculate cache affinity*/
				/* Here, we gather Resources from the lower priority tasks and also keep track of 
				 the contexts whose resources has been allocated */
										
				//	victimLaneGroups = 	vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxAllocatedLane;
			//		victimCores      =	vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxAllocatedResource;
				//	availableCores 	 += 	vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxAllocatedResource;
					
				//	if( availableCores < vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource){

						for( i = (HardwareContexts -1 - uHC) ;i > j ; i--){
							
						
								
							if( orderedContexts[i] != unusedContext){	
			
								victimLaneGroups = victimLaneGroups | vSotwareContextToHardwareContextMapping[orderedContexts[i]]->uxAllocatedLane;
								vSotwareContextToHardwareContextMapping[orderedContexts[i]]->uxAllocatedLane = 0x000;
								victimCores      +=	vSotwareContextToHardwareContextMapping[orderedContexts[i]]->uxAllocatedResource;
								availableCores 	 += 	vSotwareContextToHardwareContextMapping[orderedContexts[i]]->uxAllocatedResource;
								vSotwareContextToHardwareContextMapping[orderedContexts[i]]->uxAllocatedResource = 0x000;
								tempuHC++;
							}
							if( availableCores >= vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource)
								break;

						}
						
						if( victimCores == 0)
							tempuHC = 0U;
				// }
				
					tempMask = vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource;

					if( availableCores < vSotwareContextToHardwareContextMapping[orderedContexts[j]]->uxRequestedResource){
						tempMask = availableCores;
					}

					uHC += tempuHC;	
					availableLaneGroups  =availableLaneGroups |victimLaneGroups;
					configWords = configWords  & ~(availableLaneGroups*0xF);
                                        configWords = configWords | availableLaneGroups*8;
				        cacheAffinityMask =  availableLaneGroups*0xF;  // here still needs some consideration becuase it may jump to other lane groups
					
					
				}













                                                                                                                                                    //** is zero then adjust it resources

			tempSwitch = tempMask ;
			unsigned int mask ;
			unsigned int  config_mask;
			unsigned int way4Config = pdFALSE;
			
			way4Config = pdFALSE ;


			switch(tempSwitch){
				case 1:
					 mask = 0x0001 ;
				 	 config_mask = 0xF;
					 break;
				case 2:  
					mask = 0x0011; 

					config_mask = 0xFF;
					way4Config = pdTRUE; 
					break;
				case 3: 
				/* As avilable harware LAN Groups could be 3 .. but  we can not  merge three lan groups 
				 * to get six issue width.. therefore if some task required six issue width we will map 
				 * internally to the 4 issue width request */ 

					 mask = 0x0011;
				 	config_mask = 0xFF;
				 	way4Config = pdTRUE;
				/* Now the number of required Cores should also be 2*/
				 	tempMask   = 2U; 
					break;

				case 4:
					mask = 0x1111;
					config_mask= 0xFFFF;
					break;
				default: 
					mask = 0x000;
		   }
						
		   unsigned int temp;						
	           temp = availableLaneGroups; 
		   unsigned int temp_result;
		  

		   if(way4Config == pdTRUE){ 
			/* if any task wants  4 issue width then we can not fuse together  the middle  
		 	 * two lane groups to get the issue width of 4 due to the current design of 
			 * the core. The  2 Lane groups at the border  can only be fused together to get the 
			 * issue width 4.. that is why with the following code this case is explicitly 
		 	 *  handled
	        	*/

			for( k = 0 ; k < 2 ; k++){
			
				temp_result = mask & temp & (cacheAffinityMask) ;
                               	if( temp_result == mask ){
	
					resourceAcquired = pdTRUE;
					break;
                                                              
				}else{
					mask = mask << 8;
                                       	config_mask = config_mask << 8;
	
				}
			}
			if(resourceAcquired != pdTRUE ){
					/* it possible that  even though two lane groups are avaiable 
					 * but these lan groups are not at the border .. then they are
					 * not assigned to any task rather the resource  requirements 
					 * for that task is reduced to 2 issue width internally
					*/	

				way4Config  = pdFALSE; // Becuase required resource not avaiable so fall back to second best option
				mask = 0x0001U ;
                               	config_mask = 0xFU;
				tempMask = 1U ; 								
			}
		}

		// The following configuration will find resource for 2 and 8 issue with
		if( way4Config == pdFALSE){
		
			for( k = 0 ; k < HardwareContexts; k++) {
				temp_result = mask & temp & cacheAffinityMask ;
				if( temp_result == mask ){
					resourceAcquired = pdTRUE; 
					break;
				}
				else {
					mask = mask << 4;
					config_mask = config_mask << 4;
				}
			}
		}
	
			
	
		if(resourceAcquired == pdFALSE ) { 
							
		/* Right now, i can not come up with any  condition  when this case will
		 * execute. Therefore, i dont know what to do.. This implementation is	
		 * kept open for the future...
		*/
			
			puts(" Can not find the requested resources &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& \n");
			break;
	
		}
		else{

		/* Here all resource allocation/deallocation data structures are sey*/
			//if( j == 0){
			 //	preHighestPriorityCores = tempMask;
				//preHighestMask = mask*0xF;
				
			//}
			vSotwareContextToHardwareContextMapping[orderedContexts[j]] -> uxAllocatedLane = mask; 
			vSotwareContextToHardwareContextMapping[orderedContexts[j]] -> uxAllocatedResource = tempMask;
			availableCores = availableCores -  vSotwareContextToHardwareContextMapping[orderedContexts[j]] -> uxAllocatedResource;
			tempCores = tempCores -  vSotwareContextToHardwareContextMapping[orderedContexts[j]] -> uxAllocatedResource;
			availableLaneGroups  =availableLaneGroups ^ mask;
			resourceAcquired  = pdFALSE ; 
							
			if( orderedContexts[j] == 0U ){
				pxCT0TaskTCB = vSotwareContextToHardwareContextMapping[orderedContexts[j]];//=pxCurrentTCB; // Assignment of the task to the context zero
			}
			else { 
				*RSC_Register[orderedContexts[j]] = vSotwareContextToHardwareContextMapping[orderedContexts[j]];//=xCurrentTCB ; //  Assignment of the tasks to the contxts other than zero
			}
			temp_config = mask * orderedContexts[j];
			configWords = configWords & (~config_mask);
			configWords = configWords + temp_config; 
			

		}
			//puts("search point \n");
		
			
		if(tempCores == 0x0000U)
			break;
					
		}

	}
		
	
	//		contextSync[0] = pdTRUE; 
	contextSync[0]=pdFALSE;
	WCFG_Register[0] = 0x0000;
	SAWC_Register[0] = 1;
	CRR_Register[0]=configWords;

//	displayConfig(configWords);
/*	puts("Active configuration    ");
	tohex(st, configWords);
	puts(st);*/
	  
	
//	CRR_Register[0]=0x0000;  
}		
