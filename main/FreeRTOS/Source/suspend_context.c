//nclude "FreeRTOS.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "StackMacros.h"
#include "platform.h"

/**********************************************************************/
/********* The following are the global data structures***************/
/*********************************************************************/
/*This data structure contains TCBs of currently running tasks*/ 
extern  TCB_t* volatile  vSotwareContextToHardwareContextMapping[HardwareContexts]; 
extern   phyResource vHardwareContextToPhysicalResourceMapping[HardwareContexts] ;// This data structure contains TCBs of currently running tasks 
unsigned int  orderedContexts[HardwareContexts];
unsigned int runningContext[HardwareContexts];
extern   UBaseType_t availableCores ;// As, At maximum, foure cores are avaiable so
extern   UBaseType_t availableLanGroups ;// As, At maximum, foure cores are avaiable so 
extern unsigned int configWords;


/*
 * retrieve Resources function taskes back all the resources 
 * which are flagged free by the contexts
*/



/* calculate the rank of the gven task*/
void contextRank(void){
	unsigned int i,j;
	unsigned int rank = 1U;
	char st[15];
	unsigned int temp_flag=0U;
	/* The loop for the rank determination starts 1
	 * because 0 context is the master context and 
	 * no other context  can suspend 0th context */

	for( i = 0 ; i < HardwareContexts;i++){
		orderedContexts[i] = 100; // All Contexts are available
	}
	
	/*0th position of the ordered context contains 
	 * 0th hardwarecontext becuase 0 is  the highest rank
	 *  and hardware context 0 is master context
        */
	
//	orderedContexts[0]= 0;

/*	for( i = 0 ; i < HardwareContexts; i++){
		puts( "hardware context  ");
		tohex(st, i);
		puts(st);
		tohex(st, vSotwareContextToHardwareContextMapping[i]-> uxPriority);
		puts(st);
	}*/


		
	for( i = 0 ; i < HardwareContexts; i++){
		rank = 0;
		temp_flag=0U;
		for( j = 0 ; j < HardwareContexts;j++){

			if(( (vSotwareContextToHardwareContextMapping[j]-> uxPriority) > (vSotwareContextToHardwareContextMapping[i]-> uxPriority)) 
				&& vSotwareContextToHardwareContextMapping[i] != NULL ){
					rank++;
			
			
			}
			else if(vSotwareContextToHardwareContextMapping[i] == NULL){
				temp_flag = 1U;
				break;
			}
			
		}
		 
		if( temp_flag == 0U)
			orderedContexts[rank] = i ;	
	
	}
	
	

}

/* tell at which positions task are less priority*/
/*  and calculate how many contexts are needed to 
 * be stopped before before the requirement of the tasks are satis stasfied
*/



