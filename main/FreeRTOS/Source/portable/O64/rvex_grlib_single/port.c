/*
    FreeRTOS V8.2.3 - Copyright (C) 2015 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

    ***************************************************************************
    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<
    ***************************************************************************

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available on the following
    link: http://www.freertos.org/a00114.html

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that is more than just the market leader, it     *
     *    is the industry's de facto standard.                               *
     *                                                                       *
     *    Help yourself get started quickly while simultaneously helping     *
     *    to support the FreeRTOS project by purchasing a FreeRTOS           *
     *    tutorial book, reference manual, or both:                          *
     *    http://www.FreeRTOS.org/Documentation                              *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
    the FAQ page "My application does not run, what could be wrong?".  Have you
    defined configASSERT()?

    http://www.FreeRTOS.org/support - In return for receiving this top quality
    embedded software for free we request you assist our global community by
    participating in the support forum.

    http://www.FreeRTOS.org/training - Investing in training allows your team to
    be as productive as possible as early as possible.  Now you can receive
    FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
    Ltd, and the world's leading authority on the world's leading RTOS.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
    Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

    http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
    Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and commercial middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/

/*-----------------------------------------------------------
 * Implementation of functions defined in portable.h for the rvex port.
 *----------------------------------------------------------*/

/**
 * \file port.c
 * \brief blabla
*/

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"

/* Include private definitions file, shared between only this file and the
assembly file. */
#include "portprvmacro.h"

/* This value is used to determine whether to re-enable interrupts when leaving
a critical section. When entering a critical section or an interrupt it is
incremented and when leaving a critical section it is decremented. Only when it
is decremented to 0 will interrupts be enabled. This is stored per task. */
UBaseType_t uxPortCriticalNesting = 1;

/*
 * Assembly method which is called when a trap occurs.
 */
extern void vPortTrapHandler( void );

/*
 * Method which is called when a trap occurs which we're not capable of handling
 * (at this time).
 */
extern void vPortPanicHandler( void );

/*
 * Not really a method, but rather an assembly label which we can jump to when
 * we want to load the task context pointer to by pxCurrentTCB. Never returns.
 */
extern void vPortLoadContext( void );

/*
 * Used to catch tasks that attempt to return from their implementing function.
 * Never returns.
 */
static void prvTaskExitError( void );

/*
 * Application code interrupt handler, if defined.
 */
/*#ifdef configINTERRUPT_HANDLER
extern void configINTERRUPT_HANDLER( UBaseType_t uxInterruptId );
#endif
*/

/*-----------------------------------------------------------*/

/* 
 * See header file for description. 
 */
StackType_t *pxPortInitialiseStack( StackType_t *pxTopOfStack, TaskFunction_t pxCode, void *pvParameters )
{
	UBaseType_t uxCnt;

	/* Enforce the 32-byte stack alignment requirement imposed by the HP VEX
	RTA, even though it's a bit ridiculous and the compiler uses 8-byte
	alignment anyway. */
	pxTopOfStack = ( StackType_t * ) ( ( ( portPOINTER_SIZE_TYPE ) pxTopOfStack ) & ( ~0x0000001F ) );

	/* Allocate a 32-byte block with known values for debugging purposes. */
	pxTopOfStack -= 8;
	for( uxCnt = 0; uxCnt < 8; uxCnt++ )
	{
		pxTopOfStack[uxCnt] = 0xDEADC0DE;
	}

	/* Allocate the context block and fill it with zeroes. */
	pxTopOfStack -= portCTXT_SIZE/4;
	for( uxCnt = 0; uxCnt < portCTXT_SIZE/4; uxCnt++ )
	{
		pxTopOfStack[uxCnt] = 0;
	}

	/* Set up the context control register. */
	pxTopOfStack[portCTXT_CCR/4] = CR_CCR_IEN    /* Enable interrupts. */
								 | CR_CCR_RFT    /* Ready for trap. */
								 |CR_CCR_CSW 
								 | CR_CCR_BPE_C  /* Disable breakpoints. */
								 | CR_CCR_KME;   /* Enable kernel mode, MMU is not used. */

	/* Set the PC to the task function address. */
	pxTopOfStack[portCTXT_PC/4] = ( StackType_t ) pxCode;

	/* Make $r0.3 point to pvParameters. */
	pxTopOfStack[portCTXT_R3/4] = ( StackType_t ) pvParameters;

	/* Tasks should never return, so the return address is don't care in theory.
	However, having a task return to address 0 is a bit silly. Instead, set the
	return address to a stub which calls configASSERT(0) and then just loops
	forever, in order to fail correctly. */
	pxTopOfStack[portCTXT_LINK/4] = ( StackType_t ) prvTaskExitError;

	return pxTopOfStack;
}
/*-----------------------------------------------------------*/

BaseType_t xPortStartScheduler( void )
{
	/* Override the _start.S trap handlers with our own. */
	CR_TH = ( portBASE_TYPE ) vPortTrapHandler;
	CR_PH = ( portBASE_TYPE ) vPortPanicHandler;
	/* Jump to the end of the assembly implementation of the trap handler, which
	will restore the context from the stack pointer in the current TCB. */
	vPortLoadContext();

	/* Should not get here. */
	return pdTRUE;
}
/*-----------------------------------------------------------*/

void vPortEndScheduler( void )
{
	/* The rvex has a stop mechanism, so let's use it here. */
	//asm volatile ( "stop \n ;;" );
	_stop();
}
/*-----------------------------------------------------------*/

void vPortEnterCritical( void )
{
    portDISABLE_INTERRUPTS();
    uxPortCriticalNesting++;
}
/*-----------------------------------------------------------*/

void vPortExitCritical( void )
{
	configASSERT( uxPortCriticalNesting );
    uxPortCriticalNesting--;
    if( uxPortCriticalNesting == 0 )
    {
        portENABLE_INTERRUPTS();
    }
}

/*-----------------------------------------------------------*/

static void prvTaskExitError( void )
{
	/* A function that implements a task must not exit or attempt to return to
	its caller as there is nothing to return to.  If a task wants to exit it
	should instead call vTaskDelete( NULL ).

	Artificially force an assert() to be triggered if configASSERT() is
	defined, then stop here so application writers can catch the error. */
	configASSERT( uxPortCriticalNesting == ~0UL );
	//asm volatile ( "stop \n ;;" );
    _stop();
}
/*-----------------------------------------------------------*/

/*
 * Panic handler.
 */
void vPortPanicHandler( void )
{
	/* This is called when a panic occurs. That is, a trap occured which we
	couldn't handle at the time or can't handle at all.
	
	Artificially force an assert() to be triggered if configASSERT() is
	defined, then stop here so application writers can catch the error. */
	configASSERT( uxPortCriticalNesting == ~0UL );
	//asm volatile ( "stop \n ;;" );
    _stop();
}
/*-----------------------------------------------------------*/

/**
 * Interrupt handler.
 *\brief thisisisisi
 */
extern volatile unsigned  int *CNT_Register;	

unsigned int initValue;
unsigned int finalValue;
unsigned int result1; 

void vPortInterrupt( UBaseType_t uxInterruptId )
{
	
	char st[15];
	/*tohex(st, uxInterruptId );
	puts(st);*/
	//puts(" this is from the Isr#####################################################\n");
	heartBeat();
//	if (uxInterruptId == 0x09)	
//		asm("stop \n ;;");

	

    initValue  = CNT_Register[0];


	#ifdef configTICK_IRQ
	if( uxInterruptId == configTICK_IRQ )
	{
		
		/* Tick interrupt. */
		if( xTaskIncrementTick() != pdFALSE )
		{

			/* Here there is a need to select more tasks  rather than just one*/
	//		puts(" This is before context:) \n");

	//		vTaskSwitchContext();
		}
			vTaskSwitchContext();  //This is not optimised  implementation
	}
	#ifdef configINTERRUPT_HANDLER
	else
	#endif
	#endif
	#ifdef configINTERRUPT_HANDLER
	{
		/* Go to application code for any other interrupt. */
		//configINTERRUPT_HANDLER( uxInterruptId );
		//asm("stop \n ;;");
	}
	#endif
    finalValue = CNT_Register[0];

  if( finalValue > initValue){
	result1 = finalValue -initValue;
  }
  else{
            result1 = (0xFFFFFFFF -initValue) +finalValue;
  }
	
    puts("ISR Time ");
    tohex(st, result1);
    puts(st);




//	puts(" this is from the End of Isr ************************************************** \n");
}

/* Interrupt Enabler */


void enable_Interrupt(void)
{
		CR_CCR |=  CR_CCR_IEN |CR_CCR_RFT; 
}


