/*
    FreeRTOS V8.2.3 - Copyright (C) 2015 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

    ***************************************************************************
    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<
    ***************************************************************************

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available on the following
    link: http://www.freertos.org/a00114.html

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that is more than just the market leader, it     *
     *    is the industry's de facto standard.                               *
     *                                                                       *
     *    Help yourself get started quickly while simultaneously helping     *
     *    to support the FreeRTOS project by purchasing a FreeRTOS           *
     *    tutorial book, reference manual, or both:                          *
     *    http://www.FreeRTOS.org/Documentation                              *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
    the FAQ page "My application does not run, what could be wrong?".  Have you
    defined configASSERT()?

    http://www.FreeRTOS.org/support - In return for receiving this top quality
    embedded software for free we request you assist our global community by
    participating in the support forum.

    http://www.FreeRTOS.org/training - Investing in training allows your team to
    be as productive as possible as early as possible.  Now you can receive
    FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
    Ltd, and the world's leading authority on the world's leading RTOS.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
    Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

    http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
    Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and commercial middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/

#ifndef PORTPRVMACRO_H
#define PORTPRVMACRO_H

/*-----------------------------------------------------------
 * Port specific definitions.
 *
 * The settings in this file configure FreeRTOS correctly for the
 * given hardware and compiler.
 *
 * These settings should not be altered.
 *-----------------------------------------------------------
 */

/* The next section defines how contexts are stored on the stack. */
#define portCTXT_SIZE		0x120

/* The first 16 bytes are scratch for callees in the RTA, in this case the
callees will be the FreeRTOS kernel methods. */
#define portCTXT_SCRATCH0	0x000
#define portCTXT_SCRATCH1	0x004
#define portCTXT_SCRATCH2	0x008
#define portCTXT_SCRATCH3	0x00C

/* Context control register. */
#define portCTXT_CCR		0x010

/* Program counter. */
#define portCTXT_PC			0x014

/* General purpose registers. */
#define portCTXT_R2			0x018
#define portCTXT_R3			0x01C
#define portCTXT_R4			0x020
#define portCTXT_R5			0x024
#define portCTXT_R6			0x028
#define portCTXT_R7			0x02C
#define portCTXT_R8			0x030
#define portCTXT_R9			0x034
#define portCTXT_R10		0x038
#define portCTXT_R11		0x03C
#define portCTXT_R12		0x040
#define portCTXT_R13		0x044
#define portCTXT_R14		0x048
#define portCTXT_R15		0x04C
#define portCTXT_R16		0x050
#define portCTXT_R17		0x054
#define portCTXT_R18		0x058
#define portCTXT_R19		0x05C
#define portCTXT_R20		0x060
#define portCTXT_R21		0x064
#define portCTXT_R22		0x068
#define portCTXT_R23		0x06C
#define portCTXT_R24		0x070
#define portCTXT_R25		0x074
#define portCTXT_R26		0x078
#define portCTXT_R27		0x07C
#define portCTXT_R28		0x080
#define portCTXT_R29		0x084
#define portCTXT_R30		0x088
#define portCTXT_R31		0x08C
#define portCTXT_R32		0x090
#define portCTXT_R33		0x094
#define portCTXT_R34		0x098
#define portCTXT_R35		0x09C
#define portCTXT_R36		0x0A0
#define portCTXT_R37		0x0A4
#define portCTXT_R38		0x0A8
#define portCTXT_R39		0x0AC
#define portCTXT_R40		0x0B0
#define portCTXT_R41		0x0B4
#define portCTXT_R42		0x0B8
#define portCTXT_R43		0x0BC
#define portCTXT_R44		0x0C0
#define portCTXT_R45		0x0C4
#define portCTXT_R46		0x0C8
#define portCTXT_R47		0x0CC
#define portCTXT_R48		0x0D0
#define portCTXT_R49		0x0D4
#define portCTXT_R50		0x0D8
#define portCTXT_R51		0x0DC
#define portCTXT_R52		0x0E0
#define portCTXT_R53		0x0E4
#define portCTXT_R54		0x0E8
#define portCTXT_R55		0x0EC
#define portCTXT_R56		0x0F0
#define portCTXT_R57		0x0F4
#define portCTXT_R58		0x0F8
#define portCTXT_R59		0x0FC
#define portCTXT_R60		0x100
#define portCTXT_R61		0x104
#define portCTXT_R62		0x108
#define portCTXT_R63		0x10C

/* Branch registers. */
#define portCTXT_BR			0x110

/* Link registers. */
#define portCTXT_LINK		0x114

/* Critical section. */
#define portCTXT_CRIT		0x118

/* Alignment/reserved. */
#define portCTXT_RES0		0x11C
/*-----------------------------------------------------------*/

#endif /* PORTPRVMACRO_H */

