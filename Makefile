
WITH_MAIN = source ../../toolchain/main/sourceme &&
MAIN_ELF = main/FreeRTOS/Demo/r-VEX/demo/demo.elf

WITH_MONITOR = source ../../toolchain/monitor/sourceme && 
MONITOR_ELF = monitor/monitor.elf

INSTALL_DIR = ../../sysace/part1
INSTALL_MAIN = $(INSTALL_DIR)/freertos.1.elf
INSTALL_MONITOR = $(INSTALL_DIR)/freertos.2.elf

# Compile the program.
.PHONY: all
all: $(MAIN_ELF) $(MONITOR_ELF)
	mkdir -p $(INSTALL_DIR)
	cp $(MAIN_ELF) $(INSTALL_MAIN)
	cp $(MONITOR_ELF) $(INSTALL_MONITOR)

# Clean intermediate and output files.
.PHONY: clean
clean: main-clean monitor-clean
	rm -f $(INSTALL_MAIN) $(INSTALL_MONITOR)

# Main core compilation.
.PHONY: $(MAIN_ELF)
$(MAIN_ELF): main-all
.PHONY: main-%
main-%:
	$(WITH_MAIN) $(MAKE) -C main/FreeRTOS/Demo/r-VEX/demo $*

# Monitor core compilation.
.PHONY: $(MONITOR_ELF)
$(MONITOR_ELF): monitor-all
.PHONY: monitor-%
monitor-%:
	$(WITH_MONITOR) $(MAKE) -C monitor $*

