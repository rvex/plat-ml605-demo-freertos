
FreeRTOS demo
=============

This demo consists of a (rather jury-rigged) port of FreeRTOS that attempts to
demonstrate the reconfiguration ability of the r-VEX from a task scheduling and
energy vs. delay perspective.

FreeRTOS runs on the main core and controls the top half of the VGA/DVI output
to show the state of eight raytracer tasks. The tasks are restarted every few
minutes, the scheduler runs approximately every two seconds. The scheduler is
prioritized, starting with the raytracer task in the top-left corner of the
screen. Each task requests a randomized amount of computational resources from
the scheduler (2, 4, or 8 lanes), which the scheduler fullfills as best as it
can by descending priority, causing randomized schedules to be shown.

The secondary core runs a monitoring program that controls the bottom half of
the screen. It periodically reads the current runtime configuration and VDDcore
current consumption of the FPGA through the PMbus interface and plots these
against time. The current consumption sample is taken while the secondary core
is busy-looping, so the current consumption of the secondary core should be
relatively constant while the samples are taken.

**WARNING**: THIS VERSION OF FREERTOS SHOULD **NOT** BE USED IN SERIOUS
APPLICATIONS. THERE IS NO RESOURCE LOCKING WHATSOEVER, SO THE APPLICATION WILL
EVENTUALLY CRASH. THIS USUALLY HAPPENS EVERY HALF HOUR OR SO EVEN WITH A
TWO-SECOND TICK. IT IS INTENDED AS A DEMO/PROOF OF CONCEPT **ONLY**.



